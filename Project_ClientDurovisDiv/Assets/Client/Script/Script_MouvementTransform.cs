﻿using UnityEngine;
using System.Collections;

public class Script_MouvementTransform : MonoBehaviour 
{
	public int _NbTransform;// nb de Transform 
	public Transform Tstart;
	public Transform Tend_1;
	public Transform Tend_2;
	public Transform Tend_3;
	public Transform Tend_4;
	public Transform Tend_5;


	private Transform[] _transformArray = new Transform[50];
	private Transform end;
	private Transform start;
	public float speed = 1.0F;
	private float startTime;
	private float journeyLength;

	private int _iD;
	private int _iF;

	void Start()
	{
		_transformArray [0] = Tstart;
		_transformArray [1] = Tend_1;
		_transformArray [2] = Tend_2;
		_transformArray [3] = Tend_3;
		_transformArray [4] = Tend_4;
		_transformArray [5] = Tend_5;

		_iD = 0;
		_iF = 1;
		start = _transformArray [_iD];
		end = _transformArray [_iF];
		startTime = Time.time;
		journeyLength = Vector3.Distance(start.position, end.position);
	}

	void Update()
	{
			if (Vector3.Distance(transform.position, end.position) != 0) 
			 {
				float distCovered = (Time.time - startTime) * speed;
				float fracJourney = distCovered / journeyLength;
				transform.position = Vector3.Lerp (start.position, end.position, fracJourney);
			}
			else
			{
				if(_iF != _NbTransform-1)
				{
					_iD++;
					_iF++;
					start = _transformArray [_iD];
					end = _transformArray [_iF];
					startTime = Time.time;
					journeyLength = Vector3.Distance (start.position, end.position);
				}
			}

	}
	/*

	public Transform cam;

	float mouvement ;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		mouvement = Time.deltaTime;
		Debug.Log (mouvement);
		// This establishes basic controls relative to the camera
		Vector3 controlRight = Vector3.Cross (cam.up ,cam.forward );//(0,1,1)
		Vector3 controlForward = Vector3.Cross(cam.right, Vector3.up);//(1,1,0)
		
		// Apply movement to control input
		Vector3 controlInput = (controlRight+ controlForward);
	///


		//Vector3 mouvement = new Vector3 (mouvementX,0,mouvementZ);
		transform.Translate (controlInput * 0.005f);
	
	}*/
}
